import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=postgres user=postgres")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

add_to_inventory = """
    INSERT INTO Inventory (username, product, amount)
    VALUES (%(username)s, %(product)s, %(amount)s) 
    ON CONFLICT (username, product) DO UPDATE
    SET amount = Inventory.amount + %(amount)s;
"""

get_amount_in_inventory = """
    SELECT SUM(amount) 
    FROM Inventory 
    WHERE username = %(username)s
"""

def buy_product(username, product, amount):
    assert amount > 0, "Amount should be positive"
    
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(add_to_inventory, obj)
                cur.execute(get_amount_in_inventory, obj)
                sum_in_inventory = cur.fetchone()[0]
                if sum_in_inventory > 100:
                    raise Exception(f"Inventory should not contain 100+ elements, you want {sum_in_inventory}")
                
            except psycopg2.errors.RaiseException:
                return "Inventory should not contain 100+ elements"

            print("Success")
            conn.commit()

