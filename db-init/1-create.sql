CREATE TABLE Player (
    username TEXT primary key, 
    balance  INT CHECK(balance >= 0)
);

CREATE TABLE Shop (
    product  TEXT primary key, 
    in_stock INT CHECK(in_stock >= 0), 
    price    INT CHECK (price >= 0)
);

CREATE TABLE Inventory
(
    username TEXT REFERENCES Player,
    product  TEXT REFERENCES Shop,
    amount   INT CHECK ( amount >= 0 ),
    PRIMARY KEY (username, product)
);
